/*
 * hw_lptim.c
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#include "hw.h"
#include "utilities.h"

LPTIM_HandleTypeDef hlptim;



void HW_LPTIM_Init(void)
{

  hlptim.Instance = LPTIM1;
  hlptim.Init.Clock.Source = LPTIM_CLOCKSOURCE_ULPTIM;
  hlptim.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
  hlptim.Init.UltraLowPowerClock.Polarity = LPTIM_CLOCKPOLARITY_FALLING;
  hlptim.Init.UltraLowPowerClock.SampleTime = LPTIM_CLOCKSAMPLETIME_DIRECTTRANSITION;
  hlptim.Init.Trigger.Source = LPTIM_TRIGSOURCE_SOFTWARE;
  hlptim.Init.OutputPolarity = LPTIM_OUTPUTPOLARITY_HIGH;
  hlptim.Init.UpdateMode = LPTIM_UPDATE_IMMEDIATE;
  hlptim.Init.CounterSource = LPTIM_COUNTERSOURCE_EXTERNAL;
  if (HAL_LPTIM_Init(&hlptim) != HAL_OK)
  {
    Error_Handler();
  }

}


void HW_LPTIM_Counter_Stop_IT(void)
{
	HAL_LPTIM_Counter_Stop_IT(&hlptim);
}
void HW_LPTIM_Counter_Start_IT(int ms)
{
	HAL_LPTIM_Counter_Start_IT(&hlptim, ms);
}
void HW_LPTIM_Counter_Stop(void)
{
	HAL_LPTIM_Counter_Stop(&hlptim);
}
void HW_LPTIM_Counter_Start(int ms)
{
	HAL_LPTIM_Counter_Start(&hlptim, ms);
}


int HW_LPTIM_ReadCounter(void)
{
	return HAL_LPTIM_ReadCounter(&hlptim);
}

/**
  * @brief This function handles LPTIM1 global interrupt / LPTIM1 wake-up interrupt through EXTI line 29.
  */
void LPTIM1_IRQHandler(void)
{
  /* USER CODE BEGIN LPTIM1_IRQn 0 */

  /* USER CODE END LPTIM1_IRQn 0 */
  HAL_LPTIM_IRQHandler(&hlptim);
  /* USER CODE BEGIN LPTIM1_IRQn 1 */

  /* USER CODE END LPTIM1_IRQn 1 */
}



