/*
 * hw_flash.c
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */


#include "hw.h"
#include "bsp.h"
#include "utilities.h"



void HW_BAT_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct1 = {0};
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*inizializzo pin per lettura con ADC*/
	GPIO_InitStruct1.Pin = GPIO_PIN_4;
	GPIO_InitStruct1.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct1.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct1);


	HW_AdcInit();

	/// INPUT SELECOR
	BSP_MUX_ADC_Init();
}
int HW_BAT_Read(void)
{
	float SupplyVoltage, BatteryVoltage;
	int BatteryLevel;

	/*abilito il partitore*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);

	/*aspetto che si carichi la capacità (200ms consentono alla capacità di caricarsi, ma
	 * per essere sicuri che sia carica ne ho aggiunti altri 50ms*/
	HAL_Delay(400);

	/*inizio la lettura*/
	SupplyVoltage=HW_AdcReadChannel(ADC_CHANNEL_VREFINT); //acquisizione tensione supply per normalizzazione
	BatteryVoltage=HW_AdcReadChannel(ADC_CHANNEL_4); //pin PA4

	/*	fine lettura, disabilito il partitore*/
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);

	/*converto il valore letto in volt*/
	SupplyVoltage=( (uint32_t)VDDA_VREFINT_CAL * (*VREFINT_CAL) ) / (uint32_t)SupplyVoltage;
	BatteryVoltage=(BatteryVoltage/4096)*SupplyVoltage;

	/*
	 * tensione massima 4800mV (4 pile da 1200mV)
	 * tensione minima 4230mV (più in giù si spegne il regolatore
	 * (4800-4230)/100=5.7 mV corrispondono all'1%
	 * BatteryVoltage in realtà è il valore misurato dopo la caduta sul partitore resistivo.
	 * Per risalire al vero valore della batteria basta dividere per R2/(R1+R2)
	 *
	 * R2/(R1+R2)=0.6875 con R2=3.3 MOhm e R1=1.5 MOhm in modo da abbassare la tensione batteria
	 * da 4800mV a 3300mV
	 *
	 * */

	/*converto la tensione in livello percentuale di carica (da 0 a 100)*/
	//BatteryLevel=(int)((BatteryVoltage/0.5)-4230)/5.7;


	/*
	 * tensione massima 4500mV (3 pile da 1500mV)
	 * tensione minima 3800mV (più in giù si spegne il regolatore
	 * (4500-3800)/100=7mV corrispondono all'1%
	 * BatteryVoltage in realtà è il valore misurato dopo la caduta sul partitore resistivo.
	 * Per risalire al vero valore della batteria basta dividere per R2/(R1+R2)
	 *
	 * R2/(R1+R2)=0.5 con R2=1.5 MOhm e R1=1.5 MOhm in modo da abbassare la tensione batteria
	 * da 4500mV a 2250mV
	 *
	 * */
	BatteryLevel=(int)((BatteryVoltage/0.5)-3800)/7;

	if(BatteryLevel>=100){
		BatteryLevel=100;
	}
	if(BatteryLevel<=0){
		BatteryLevel=0;
	}
	return BatteryLevel;
}
