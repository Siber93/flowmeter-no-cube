#include "flow_meter_manager.h"
#include "timeServer.h"
#include "hw.h"
#include "bsp.h"
#include "lora.h"


// Next system state that is gonna be set on next while(1) iteration
enum GLOBAL_STATES next_state = IDLE;

// Last sensor value red
int old_sensor_value = 0;

// Counter of RTC tick without floodingsession_status
int empty_flood_cnt = 0;

// Event that elaborates LPTIM counter
TimerEvent_t rtcStreamTimer;

// Event that elaborates LPTIM counter
TimerEvent_t rtcDailyTimer;


void SetDefaultVariablesState(void);

void DailySendExecute();



void FloodingBegining(void)
{
	// The flooding has to start
	next_state = FLOODING;

	// Back to STOP MODE
	LowPowerModeActive = 0;
	//SendData(10000);
}


void DailySendExecute(void)
{
	//HW_FLASH_Set_Impulse_Counter(HW_FLASH_Get_Impulse_Counter()+ old_sensor_value);
	NeedToSend = 1;
	TimerReset(&rtcDailyTimer);
}

void ScheduledCallback(void)
{
	switch(current_state)
	{
	case IDLE:
		// Reset the 3 ticks timer counter
		HW_LPTIM_Counter_Stop_IT();
		HW_LPTIM_Counter_Start_IT(IT_TIMER_PERIOD);
		// Genero il burst di 5 impulsi per attivare LPTIM
		BSP_Burst_Generate();
		break;

	case FLOODING: ;
		// read LPTIM counter value
		int new_sensor_value = HW_LPTIM_ReadCounter();

		// check if something is changed from last reading
		if(old_sensor_value != new_sensor_value)
		{
			old_sensor_value = new_sensor_value;
			empty_flood_cnt = 0;
#ifdef ON_TICK_SEND
			// Send new value + IT_TIMER_PERIOD + 1

			NeedToSend = 1;
			// Imposto lo stato di sessione a 2
			session_status = 2;
			cumulative_counter = HW_FLASH_Get_Impulse_Counter()+ old_sensor_value + IT_TIMER_PERIOD + 1;

#endif

		}
		else
		{
			// Nothing has changed
			empty_flood_cnt++;

			if(empty_flood_cnt > EMPTY_FLOODS_LIMIT)
			{
				// Send new value + 4

				cumulative_counter = HW_FLASH_Get_Impulse_Counter()+ old_sensor_value + IT_TIMER_PERIOD + 1;
				HW_FLASH_Set_Impulse_Counter(cumulative_counter);
				NeedToSend = 3;

				// Imposto lo stato di sessione a 3
				session_status = 3;

				// Reset INITIAL STATE
				next_state = IDLE;

				// Reset Global variables
				SetDefaultVariablesState();

			}

		}
		break;
	}

	// Back to STOP MODE
	LowPowerModeActive = 0;

	TimerReset(&rtcStreamTimer);
}


void SetDefaultVariablesState(void)
{
	old_sensor_value = 0;
	empty_flood_cnt = 0;
}

// Set the new state after after have done necessary actions
void SetNextState(void)
{
	if(current_state != next_state)
	{
		// Stop RTC alarm interrupt
		TimerStop(&rtcStreamTimer);
		//HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_A);
		// Stop Timer counter
		HW_LPTIM_Counter_Stop_IT();
		switch(next_state)
		{
			case IDLE:

				// Riattivo invio della giornata
				TimerStart(&rtcDailyTimer);

				// Start Timer counter [INTERRUPT]
				HW_LPTIM_Counter_Start_IT(IT_TIMER_PERIOD);
				// Genero il burst di 5 impulsi per attivare LPTIM
				BSP_Burst_Generate();
				// Reset rtc alarm to slow mode (1 h)
				TimerStop(&rtcStreamTimer);
				TimerSetValue(&rtcStreamTimer, THRESHOLD_RESET_MS_TIMESPAN);
				TimerStart(&rtcStreamTimer);
				break;

			case FLOODING:

				// Blocco invio della giornata durante la sessione di irrigazione
				TimerStop(&rtcDailyTimer);

				// Start Timer counter [NO INTERRUPT]
				HW_LPTIM_Counter_Start(NO_IT_TIMER_PERIOD);
				// Genero il burst di 5 impulsi per attivare LPTIM
				BSP_Burst_Generate();
				// Reset RTC and set alarm to 2 min
				TimerStop(&rtcStreamTimer);
				TimerSetValue(&rtcStreamTimer, IRRIGATION_INTERVAL_TIME);
				TimerStart(&rtcStreamTimer);

				HW_FLASH_Set_Session_Counter(HW_FLASH_Get_Session_Counter()+1);
				break;
		}
		current_state = next_state;
	}
}

void InitFlowCounter(void)
{
	current_state = IDLE;
	NeedToSend = 0;

	// Start RTC Timer 1 hour, at expiration it clears the LPTIM counter register
	TimerInit(&rtcStreamTimer, ScheduledCallback);
	TimerSetValue(&rtcStreamTimer, THRESHOLD_RESET_MS_TIMESPAN);
	TimerStart(&rtcStreamTimer);

	// Start RTC Timer 1 Day
	TimerInit(&rtcDailyTimer, DailySendExecute);
	TimerSetValue(&rtcDailyTimer, MS_IN_DAY);
	TimerStart(&rtcDailyTimer);


	// Start LPTIM counting impulses till threshold
	HW_LPTIM_Counter_Start_IT(IT_TIMER_PERIOD);

	// Genero il burst di 5 impulsi per attivare LPTIM
	BSP_Burst_Generate();

	// Ottengo il valore del contatore cumulativo dalla flash
	cumulative_counter = HW_FLASH_Get_Impulse_Counter();

	session_status = 0;

}

void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
	// The flooding has to start
	next_state = FLOODING;

	// Back to STOP MODE
	LowPowerModeActive = 0;

	// Imposto lo stato di sessione a 1
	session_status = 1;

	// Forzo l'invio del primo pacchetto
	NeedToSend = 3;

}
