/*
 * hw_flash.c
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#include "hw.h"
#include "utilities.h"

#define FLASH_IMPULSE_COUNTER_ADDR 0x8017F80 // sector 47
#define FLASH_SESSION_COUNTER_ADDR FLASH_IMPULSE_COUNTER_ADDR + 8 // sector 47


#define ImpulseCounter (*(uint64_t*)FLASH_IMPULSE_COUNTER_ADDR)

#define SessionCounter (*(uint64_t*)(FLASH_SESSION_COUNTER_ADDR))

static uint8_t Write_Flash(uint32_t value, uint32_t addr);

void HW_FLASH_Init(void)
{
	// Unlock the Program memory
	HAL_FLASH_Unlock();

	// Clear all FLASH flags
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_WRPERR |
			FLASH_FLAG_OPTVERR |
			FLASH_FLAG_PGAERR |
			FLASH_FLAG_EOP);

	// Unlock the Program memory
	HAL_FLASH_Lock();

	/*int index = 0;
	// Read impulse counter
	for(int i = sizeof(ImpulseCounter)-1; i > 0; i--)
	{
		ImpulseCounter += (uint64_t)(userPersistentData[index++]<<i);
	}

	// Read Message Counter
	for(int i = sizeof(MessageCounter)-1; i > 0; i--)
	{
		MessageCounter += (uint64_t)(userPersistentData[index++]<<i);
	}*/
}



uint64_t HW_FLASH_Get_Impulse_Counter(void)
{
	return ImpulseCounter;
}
void HW_FLASH_Set_Impulse_Counter(uint64_t value)
{
	FLASH_EraseInitTypeDef desc;
	uint32_t result 		= 0;

	HAL_FLASH_Unlock();

	//desc.Banks			 = FLASH_BANK_1;
	desc.PageAddress = FLASH_IMPULSE_COUNTER_ADDR;
	desc.TypeErase 	 = FLASH_TYPEERASE_PAGES;
	desc.NbPages 		 = 1;

	uint64_t messageCounter = SessionCounter;

	HAL_FLASHEx_Erase(&desc, &result);

	Write_Flash(value, FLASH_IMPULSE_COUNTER_ADDR);
	Write_Flash(value>>32, FLASH_IMPULSE_COUNTER_ADDR+4);

	Write_Flash(messageCounter, FLASH_SESSION_COUNTER_ADDR);
	Write_Flash(messageCounter>>32, FLASH_SESSION_COUNTER_ADDR+4);

	HAL_FLASH_Lock();
}

uint64_t HW_FLASH_Get_Session_Counter(void)
{
	return SessionCounter;
}
void HW_FLASH_Set_Session_Counter(uint64_t value)
{
	FLASH_EraseInitTypeDef desc;
		uint32_t result 		= 0;

		HAL_FLASH_Unlock();

		//desc.Banks			 = FLASH_BANK_1;
		desc.PageAddress = FLASH_IMPULSE_COUNTER_ADDR;
		desc.TypeErase 	 = FLASH_TYPEERASE_PAGES;
		desc.NbPages 		 = 1;

		uint64_t impulseCounter = ImpulseCounter;

		HAL_FLASHEx_Erase(&desc, &result);

		Write_Flash(impulseCounter, FLASH_IMPULSE_COUNTER_ADDR);
		Write_Flash(impulseCounter>>32, FLASH_IMPULSE_COUNTER_ADDR+4);

		Write_Flash(value, FLASH_SESSION_COUNTER_ADDR);
		Write_Flash(value>>32, FLASH_SESSION_COUNTER_ADDR+4);

		HAL_FLASH_Lock();
}



static uint8_t Write_Flash(uint32_t value, uint32_t addr)
{
	if(HAL_FLASH_Program(TYPEPROGRAM_WORD, addr, value) == HAL_OK)
	{
		/* Check the written value */
		if(*(uint32_t*)addr != value)
		{
			/* Flash content doesn't match source content */
			return 1;
		}

	}
	else
	{
		/* Error occurred while writing data into Flash */
		return 1;
	}
	return 0;
}



void HW_FLASH_Reset(void)
{
	HW_FLASH_Set_Impulse_Counter(0);
	HW_FLASH_Set_Session_Counter(0);
}
