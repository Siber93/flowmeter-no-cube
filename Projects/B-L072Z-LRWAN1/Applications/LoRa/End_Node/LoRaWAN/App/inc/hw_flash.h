/*
 * hw_flash.h
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#ifndef __HW_FLASH_H__
#define __HW_FLASH_H__

void HW_FLASH_Init(void);

uint64_t HW_FLASH_Get_Impulse_Counter(void);
void HW_FLASH_Set_Impulse_Counter(uint64_t value);

uint64_t HW_FLASH_Get_Session_Counter(void);
void HW_FLASH_Set_Session_Counter(uint64_t value);

void HW_FLASH_Reset(void);




#endif /* PROJECTS_END_NODE_HW_LPTIM_H_ */
