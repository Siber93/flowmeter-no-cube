/*
 * hw_lptim.h
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#ifndef __HW_LPTIM_H__
#define __HW_LPTIM_H__

void HW_LPTIM_Init(void);
void HW_LPTIM_Counter_Stop_IT(void);
void HW_LPTIM_Counter_Start_IT(int ms);
void HW_LPTIM_Counter_Stop(void);
void HW_LPTIM_Counter_Start(int ms);
int HW_LPTIM_ReadCounter(void);
void LPTIM1_IRQHandler(void);

#endif /* PROJECTS_END_NODE_HW_LPTIM_H_ */
