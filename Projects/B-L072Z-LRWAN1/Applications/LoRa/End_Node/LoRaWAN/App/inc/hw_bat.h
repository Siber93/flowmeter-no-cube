/*
 * hw_lptim.h
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#ifndef __HW_BAT_H__
#define __HW_BAT_H__

void HW_BAT_Init(void);
int HW_BAT_Read(void);

#endif /* PROJECTS_END_NODE_HW_LPTIM_H_ */
