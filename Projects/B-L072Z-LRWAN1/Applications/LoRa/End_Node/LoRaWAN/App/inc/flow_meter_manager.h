/*
 * flow_meter_manager.h
 *
 *  Created on: Sep 24, 2019
 *      Author: siber
 */

#include <stdint.h>

#ifndef __FLOW_METER_MANAGER_H__
#define __FLOW_METER_MANAGER_H__

enum GLOBAL_STATES
{
	IDLE = 0,
	FLOODING = 1
} GLOBAL_STATES;


#define NO_IT_TIMER_PERIOD 65530 // Massimo numero di conteggio quando e' attiva l'irrigazione
#define IT_TIMER_PERIOD 3 // Al 4 impulso il micro si risveglia
#define EMPTY_FLOODS_LIMIT 0 // Numero di intervalli senza transizione di acqua nella conduttura dopo i quali il micro torna nello stato di idle

#define IRRIGATION_INTERVAL_TIME (4*60*1000)


#define THRESHOLD_RESET_MS_TIMESPAN (60 * 60 * 1000)

#define MS_IN_DAY (24 * 60 * 60 * 1000)

// Current system state
enum GLOBAL_STATES current_state;

// indicates when the micro is in stop mode
int LowPowerModeActive;

// Indicates when it's time to send
int NeedToSend;

uint64_t cumulative_counter;

uint8_t session_status;


void InitFlowCounter(void);


void FloodingBegining(void);

void ScheduledCallback(void);

void SetNextState(void);



#endif /* PROJECTS_END_NODE_FLOW_METER_MANAGER_H_ */
