EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FlowMeter:Conn_BL072ZLRWAN1 CN1
U 1 1 5E1B8E4D
P 7800 4100
F 0 "CN1" H 8028 4091 50  0000 L CNN
F 1 "Conn_BL072ZLRWAN1" H 8028 4000 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x26_P2.54mm_Vertical" H 7800 4100 50  0001 C CNN
F 3 "~" H 7800 4100 50  0001 C CNN
	1    7800 4100
	1    0    0    -1  
$EndComp
$Comp
L FlowMeter:Conn_BL072ZLRWAN1 CN2
U 1 1 5E1BD33D
P 10000 4100
F 0 "CN2" H 10228 4091 50  0000 L CNN
F 1 "Conn_BL072ZLRWAN1" H 10228 4000 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x26_P2.54mm_Vertical" H 10000 4100 50  0001 C CNN
F 3 "~" H 10000 4100 50  0001 C CNN
	1    10000 4100
	-1   0    0    -1  
$EndComp
$Comp
L FlowMeter:STLQ015 U5
U 1 1 5E1D2E18
P 4800 4100
F 0 "U5" H 4825 4175 50  0000 C CNN
F 1 "STLQ015" H 4825 4084 50  0000 C CNN
F 2 "FlowMeter:STLQ015M33R-SOT23" H 4800 4100 50  0001 C CNN
F 3 "" H 4800 4100 50  0001 C CNN
	1    4800 4100
	1    0    0    -1  
$EndComp
$Comp
L FlowMeter:TS5A3359 U2
U 1 1 5E1D4DA3
P 2800 1200
F 0 "U2" H 2825 1381 50  0000 C CNN
F 1 "TS5A3359" H 2825 1290 50  0000 C CNN
F 2 "FlowMeter:TS5A3359DCUT-SOP" H 2800 1200 50  0001 C CNN
F 3 "" H 2800 1200 50  0001 C CNN
	1    2800 1200
	1    0    0    -1  
$EndComp
Text Label 7800 2950 0    50   ~ 0
TCX0
Text Label 7800 3050 0    50   ~ 0
MCU
Text Label 7800 3150 0    50   ~ 0
GND
Text Label 7800 3250 0    50   ~ 0
RF
Text Label 7800 3350 0    50   ~ 0
GND
Text Label 7800 3450 0    50   ~ 0
USB
Text Label 7800 3550 0    50   ~ 0
GND
Text Label 7800 3650 0    50   ~ 0
BOOT0
Text Label 7800 3750 0    50   ~ 0
PA13
Text Label 7800 3850 0    50   ~ 0
PA14
Text Label 7800 3950 0    50   ~ 0
DI00
Text Label 7800 4050 0    50   ~ 0
DI01
Text Label 7800 4150 0    50   ~ 0
DI02
Text Label 7800 4250 0    50   ~ 0
DI03
Text Label 7800 4350 0    50   ~ 0
DI04
Text Label 7800 4450 0    50   ~ 0
DI05
Text Label 7800 4550 0    50   ~ 0
GND
Text Label 7800 4650 0    50   ~ 0
NRST
Text Label 7800 4750 0    50   ~ 0
3V3
Text Label 7800 4850 0    50   ~ 0
5V
Text Label 7800 4950 0    50   ~ 0
VIN
Text Label 7800 5050 0    50   ~ 0
GND
Text Label 7800 5150 0    50   ~ 0
PA0
Text Label 7800 5250 0    50   ~ 0
PA4
Text Label 7800 5350 0    50   ~ 0
PH1
Text Label 7800 5450 0    50   ~ 0
PH0
Text Label 9800 2950 0    50   ~ 0
CRF1
Text Label 9800 3050 0    50   ~ 0
CRF2
Text Label 9800 3150 0    50   ~ 0
CRF3
Text Label 9800 3250 0    50   ~ 0
STSAFE_NRST
Text Label 9800 3350 0    50   ~ 0
AVDD
Text Label 9800 3450 0    50   ~ 0
GND
Text Label 9800 3550 0    50   ~ 0
PA5
Text Label 9800 3650 0    50   ~ 0
PB13
Text Label 9800 3750 0    50   ~ 0
PB14
Text Label 9800 3850 0    50   ~ 0
PB15
Text Label 9800 3950 0    50   ~ 0
PB6
Text Label 9800 4050 0    50   ~ 0
GND
Text Label 9800 4150 0    50   ~ 0
PA9
Text Label 9800 4250 0    50   ~ 0
PA12
Text Label 9800 4350 0    50   ~ 0
PA11
Text Label 9800 4550 0    50   ~ 0
PB2
Text Label 9800 4650 0    50   ~ 0
PA8
Text Label 9800 4750 0    50   ~ 0
PB7
Text Label 9800 4850 0    50   ~ 0
PB5
Text Label 9800 4950 0    50   ~ 0
PA10
Text Label 9800 5050 0    50   ~ 0
PA2
Text Label 9800 5150 0    50   ~ 0
PA3
Text Label 9800 4450 0    50   ~ 0
PB12
Text Label 9800 5250 0    50   ~ 0
PB9
Text Label 9800 5350 0    50   ~ 0
PB8
Text Label 9800 5450 0    50   ~ 0
GND
$Comp
L FlowMeter:TS5A3359 U4
U 1 1 5E1DB9B1
P 6400 1250
F 0 "U4" H 6425 1431 50  0000 C CNN
F 1 "TS5A3359" H 6425 1340 50  0000 C CNN
F 2 "FlowMeter:TS5A3359DCUT-SOP" H 6400 1250 50  0001 C CNN
F 3 "" H 6400 1250 50  0001 C CNN
	1    6400 1250
	1    0    0    -1  
$EndComp
$Comp
L FlowMeter:OnOffSwitch U1
U 1 1 5E23A6C0
P 1600 4050
F 0 "U1" H 1600 4317 50  0000 C CNN
F 1 "OnOffSwitch" H 1600 4226 50  0000 C CNN
F 2 "FlowMeter:SS12SDH2" H 1600 3950 50  0001 C CNN
F 3 "http://pdf.datasheetcatalog.com/datasheets/70/494502_DS.pdf" H 1600 4050 50  0001 C CNN
	1    1600 4050
	1    0    0    -1  
$EndComp
$Comp
L FlowMeter:BatteryHolder_3xAA U3
U 1 1 5E2427A2
P 2300 5100
F 0 "U3" H 3128 4021 50  0000 L CNN
F 1 "BatteryHolder_3xAA" H 3128 3930 50  0000 L CNN
F 2 "FlowMeter:12BH331PGR" H 2300 5100 50  0000 C CNN
F 3 "" H 2300 5100 50  0001 C CNN
	1    2300 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1900 4050 2100 4050
Text Label 1950 4050 0    50   ~ 0
GND
Wire Wire Line
	6450 1950 6450 2100
Text Label 6450 2000 0    50   ~ 0
GND
Wire Wire Line
	1600 4250 1600 4450
Wire Wire Line
	1300 4050 1100 4050
Text Label 1100 4050 0    50   ~ 0
VBAT
Text Label 1600 4400 0    50   ~ 0
VIN
Text Label 3900 4250 0    50   ~ 0
VIN
Wire Wire Line
	3850 4250 4150 4250
$Comp
L pspice:C C3
U 1 1 5E27E5F2
P 4150 4000
F 0 "C3" H 3972 3954 50  0000 R CNN
F 1 "C" H 3972 4045 50  0000 R CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4150 4000 50  0001 C CNN
F 3 "~" H 4150 4000 50  0001 C CNN
	1    4150 4000
	-1   0    0    1   
$EndComp
Connection ~ 4150 4250
Wire Wire Line
	4150 4250 4300 4250
Wire Wire Line
	4150 3800 4150 3750
Wire Wire Line
	4150 3750 4550 3750
Connection ~ 4150 3750
Text Label 4350 3750 0    50   ~ 0
GND
Wire Wire Line
	5350 4250 5750 4250
Wire Wire Line
	5350 4450 5500 4450
Text Label 5400 4450 0    50   ~ 0
GND
$Comp
L pspice:C C5
U 1 1 5E283D99
P 5750 4500
F 0 "C5" H 5928 4546 50  0000 L CNN
F 1 "C" H 5928 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5750 4500 50  0001 C CNN
F 3 "~" H 5750 4500 50  0001 C CNN
	1    5750 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 4750 5750 4850
Text Label 5750 4800 0    50   ~ 0
GND
Wire Wire Line
	4300 4450 4150 4450
Wire Wire Line
	4150 4450 4150 4250
Wire Wire Line
	5750 4250 5950 4250
Connection ~ 5750 4250
Text Label 5850 4250 0    50   ~ 0
VDD
Wire Wire Line
	1600 5100 1600 4950
Wire Wire Line
	2900 7350 2900 7550
Text Label 1600 5100 0    50   ~ 0
VBAT
Text Label 2900 7500 0    50   ~ 0
GND
Wire Wire Line
	7600 4700 7450 4700
Text Label 7450 4700 0    50   ~ 0
VDD
Wire Wire Line
	2850 1900 2850 2050
Text Label 2850 1900 0    50   ~ 0
GND
Wire Wire Line
	2600 1200 2600 1150
Wire Wire Line
	6200 1250 6200 1200
Text Label 2600 1100 0    50   ~ 0
VBAT
Text Label 6200 1150 0    50   ~ 0
VDD
$Comp
L pspice:C C1
U 1 1 5E299B6D
P 1950 1450
F 0 "C1" H 2128 1496 50  0000 L CNN
F 1 "C" H 2128 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1950 1450 50  0001 C CNN
F 3 "~" H 1950 1450 50  0001 C CNN
	1    1950 1450
	1    0    0    -1  
$EndComp
$Comp
L pspice:C C4
U 1 1 5E299E59
P 5550 1500
F 0 "C4" H 5728 1546 50  0000 L CNN
F 1 "C" H 5728 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5550 1500 50  0001 C CNN
F 3 "~" H 5550 1500 50  0001 C CNN
	1    5550 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1250 5550 1200
Wire Wire Line
	5550 1200 6000 1200
Connection ~ 6200 1200
Wire Wire Line
	6200 1200 6200 1050
Wire Wire Line
	1950 1200 1950 1150
Wire Wire Line
	1950 1150 2300 1150
Connection ~ 2600 1150
Wire Wire Line
	2600 1150 2600 1000
Text Label 5550 2000 0    50   ~ 0
GND
Text Label 1950 1950 0    50   ~ 0
GND
$Comp
L Device:R R1
U 1 1 5E2A01F8
P 3550 2000
F 0 "R1" H 3620 2046 50  0000 L CNN
F 1 "R" H 3620 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3480 2000 50  0001 C CNN
F 3 "~" H 3550 2000 50  0001 C CNN
	1    3550 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E2A06C1
P 3550 2450
F 0 "R2" H 3620 2496 50  0000 L CNN
F 1 "R" H 3620 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3480 2450 50  0001 C CNN
F 3 "~" H 3550 2450 50  0001 C CNN
	1    3550 2450
	1    0    0    -1  
$EndComp
$Comp
L pspice:C C2
U 1 1 5E2A17BA
P 3850 2550
F 0 "C2" H 4028 2596 50  0000 L CNN
F 1 "C" H 4028 2505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3850 2550 50  0001 C CNN
F 3 "~" H 3850 2550 50  0001 C CNN
	1    3850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2150 3550 2300
Wire Wire Line
	3850 2300 3550 2300
Connection ~ 3550 2300
Wire Wire Line
	3550 2600 3550 2950
Wire Wire Line
	3850 2800 3850 2950
Text Label 3550 2900 0    50   ~ 0
GND
Text Label 3850 2900 0    50   ~ 0
GND
Wire Wire Line
	3200 1450 3550 1450
Wire Wire Line
	3550 1450 3550 1850
Wire Wire Line
	2450 1750 2450 2050
Text Label 2450 2050 0    50   ~ 0
BAT_READ_EN
Wire Wire Line
	10200 4600 10400 4600
Text Label 10250 4600 0    50   ~ 0
BAT_READ_EN
Wire Wire Line
	2450 1650 2200 1650
Wire Wire Line
	2200 1650 2200 2050
Text Label 2200 1950 0    50   ~ 0
GND
Wire Wire Line
	2450 1500 2300 1500
Wire Wire Line
	2300 1500 2300 1150
Connection ~ 2300 1150
Wire Wire Line
	2300 1150 2600 1150
Wire Wire Line
	1950 1700 1950 2050
Wire Wire Line
	6050 1550 5850 1550
Wire Wire Line
	5850 1550 5850 2100
Text Label 5850 2000 0    50   ~ 0
LPTIM_IN
Wire Wire Line
	5550 1750 5550 2100
Wire Wire Line
	10200 4800 10400 4800
Text Label 10250 4800 0    50   ~ 0
LPTIM_IN
Wire Wire Line
	6050 1800 6000 1800
Wire Wire Line
	6000 1800 6000 1200
Connection ~ 6000 1200
Wire Wire Line
	6000 1200 6200 1200
Wire Wire Line
	6050 1700 5900 1700
Wire Wire Line
	5900 1700 5900 750 
Text Label 5900 850  0    50   ~ 0
LPTIM_IN_SEL
Wire Wire Line
	10200 5300 10400 5300
Text Label 10250 5300 0    50   ~ 0
LPTIM_IN_SEL
Wire Wire Line
	6800 1600 7200 1600
Text Label 6900 1600 0    50   ~ 0
BURST_GEN
Wire Wire Line
	10200 5200 10400 5200
Text Label 10250 5200 0    50   ~ 0
BURST_GEN
Wire Wire Line
	8900 1050 8900 1200
$Comp
L Device:R R4
U 1 1 5E2EB254
P 8900 1350
F 0 "R4" H 8970 1396 50  0000 L CNN
F 1 "R" H 8970 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8830 1350 50  0001 C CNN
F 3 "~" H 8900 1350 50  0001 C CNN
	1    8900 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E2EC1D8
P 8600 1500
F 0 "R3" V 8393 1500 50  0000 C CNN
F 1 "R" V 8484 1500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 8530 1500 50  0001 C CNN
F 3 "~" H 8600 1500 50  0001 C CNN
	1    8600 1500
	0    1    1    0   
$EndComp
$Comp
L pspice:C C6
U 1 1 5E2ECA29
P 8250 1750
F 0 "C6" H 8428 1796 50  0000 L CNN
F 1 "C" H 8428 1705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8250 1750 50  0001 C CNN
F 3 "~" H 8250 1750 50  0001 C CNN
	1    8250 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1500 8900 1500
Wire Wire Line
	8250 2000 8250 2200
Connection ~ 8900 1500
Text Label 8900 1150 0    50   ~ 0
VDD
Wire Wire Line
	8450 1500 8250 1500
Connection ~ 8250 1500
Wire Wire Line
	6800 1500 8250 1500
Wire Wire Line
	9350 1700 9550 1700
Text Label 9350 1700 0    50   ~ 0
GND
Wire Wire Line
	9550 1900 8900 1900
Wire Wire Line
	9550 1600 9350 1600
Text Label 9350 1600 0    50   ~ 0
GND
Wire Wire Line
	7600 3100 7450 3100
Text Label 7450 3100 0    50   ~ 0
GND
Wire Wire Line
	7600 3300 7450 3300
Text Label 7450 3300 0    50   ~ 0
GND
Wire Wire Line
	7600 3500 7450 3500
Text Label 7450 3500 0    50   ~ 0
GND
Wire Wire Line
	7600 4500 7450 4500
Text Label 7450 4500 0    50   ~ 0
GND
Wire Wire Line
	7600 5000 7450 5000
Text Label 7450 5000 0    50   ~ 0
GND
Wire Wire Line
	10350 5400 10200 5400
Text Label 10200 5400 0    50   ~ 0
GND
Wire Wire Line
	10350 4000 10200 4000
Text Label 10200 4000 0    50   ~ 0
GND
Wire Wire Line
	10350 3400 10200 3400
Text Label 10200 3400 0    50   ~ 0
GND
NoConn ~ 7600 5400
NoConn ~ 7600 5300
NoConn ~ 7600 4900
NoConn ~ 7600 4800
NoConn ~ 7600 4600
NoConn ~ 7600 4400
NoConn ~ 7600 4300
NoConn ~ 7600 4200
NoConn ~ 7600 4100
NoConn ~ 7600 4000
NoConn ~ 7600 3900
NoConn ~ 7600 3800
NoConn ~ 7600 3700
NoConn ~ 7600 3600
NoConn ~ 7600 3400
NoConn ~ 7600 3200
NoConn ~ 7600 3000
NoConn ~ 7600 2900
NoConn ~ 10200 5100
NoConn ~ 10200 5000
NoConn ~ 10200 4900
NoConn ~ 10200 4700
NoConn ~ 10200 4500
NoConn ~ 10200 4400
NoConn ~ 10200 4300
NoConn ~ 10200 4200
NoConn ~ 10200 4100
NoConn ~ 10200 3900
NoConn ~ 10200 3800
NoConn ~ 10200 3700
NoConn ~ 10200 3600
NoConn ~ 10200 3500
NoConn ~ 10200 3300
NoConn ~ 10200 3100
NoConn ~ 10200 3000
NoConn ~ 10200 2900
NoConn ~ 6800 1400
NoConn ~ 3200 1550
NoConn ~ 3200 1350
NoConn ~ 10200 3200
Text Label 8250 2100 0    50   ~ 0
GND
Wire Wire Line
	3850 2300 4150 2300
Connection ~ 3850 2300
Text Label 4050 2300 0    50   ~ 0
ADC_BAT
Wire Wire Line
	7600 5200 7450 5200
NoConn ~ 7600 5100
Text Label 7450 5200 0    50   ~ 0
ADC_BAT
$Comp
L FlowMeter:SJ-3524-SMT J1
U 1 1 5E214C8C
P 10050 1800
F 0 "J1" H 9720 1896 50  0000 R CNN
F 1 "SJ-3524-SMT" H 9720 1805 50  0000 R CNN
F 2 "FlowMeter:SJ-3524-SMT-TR" H 9700 2550 50  0001 L BNN
F 3 "Manufacturer recommendations" H 9500 2400 50  0001 L BNN
F 4 "1.03" H 10500 2550 50  0001 L BNN "Field4"
F 5 "CUI Inc" H 9950 2250 50  0001 L BNN "Field5"
	1    10050 1800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8900 1500 8900 1900
NoConn ~ 9550 1800
$EndSCHEMATC
